const Sequelize = require('sequelize');

const sequelize = require('../configs/sequelize');

class cart extends Sequelize.Model {}

cart.init({
  bookId: Sequelize.INTEGER,
  userId: Sequelize.INTEGER
}, { sequelize, modelName: 'cart' });

module.exports = cart;