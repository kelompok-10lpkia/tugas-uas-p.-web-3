const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

const Cart = require('../models/cart');
module.exports.getAll = (req, res) =>{
	Cart.findAll().then(Cart=> {
		res.json(Cart);
	}).catch((error)=>{
		console.log(error);
	});
}

module.exports.postAddCart = (req, res) =>{
	jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
				res.sendStatus(403);
		}else{
			if (authData['roles']=="user") {
				var bukuId = req.body.bukuId;
				Cart.create({
						userId: authData['id'],
						bukuId : bukuId
				})
				.then(Cart => {
						res.json(Cart);
				});
			}else{
				res.sendStatus(403);
			}
		}
	});
}