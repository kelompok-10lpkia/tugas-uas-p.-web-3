const Product = require('../models/product');
const Sales = require('../models/sales');
const SalesProduct = require('../models/sales_product');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

module.exports.getIndexProduct = (req, res) => {
	jwt.verify(req.token, process.env.SECRETKEY, (error, authData) => {
		if(error) {
			res.sendStatus(403);
		} else {
			res.json({
				message: 'OK',
				authData: authData
			})
		}
	})
}

module.exports.getProductId = (req, res) => {
	Product.findByPk(req.params.id).then(buku => {
		res.json(buku)
	})
}
module.exports.getProduct = (req, res) => {
	
	Product.findAll().then((product) => {
		res.json(product);
	})
	
	
	// Product.findOne({
	// 	where: {
	// 		name: 'barang1'
	// 	}
	// }).then((product) => {
	// 	res.json(product);
	// });
	
	// Product.findAll().then((products) => {
	// res.json(products);
	// });
	
	// Product.count().then((count) => {
	// 	res.json(count);
	// });
	
	// Product.findAndCountAll().then((result) => {
	// 	res.json(result);
	// });

}

module.exports.postProduct = (req, res) => {
	jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
			res.sendStatus(403);
		}else{
			if (authData['roles']=="admin") {
				Product.create({
					name: req.body.name,
					price: req.body.price
				}).then((product) => {
					res.json(product);
				}).catch((error) => {
					throw error;
				})
			}
		}
	})
}

module.exports.putProduct = (req, res) => {
	jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
			res.sendStatus(403);
		}else{
			if (authData['roles']=="admin") {
				Product.update({
					name: req.body.name,
					price: req.body.price
				}, {
					where: {
						id: req.params.id
					}
				}).then((product) => {
					res.json(product);
				}).catch((error) => {
					throw error;
				})
			}
		}
	}
	)
}

module.exports.deleteProduct = (req, res) => {
	Product.destroy({
		where: {
			id: req.params.id
		}
	}).then((product) => {
		res.json(product);
	}).catch((error) => {
		throw error;
	})
}