const express = require('express');

const router = express.Router();

const productController = require('../controllers/product');

const auth = require('../configs/auth');

router.get('/', productController.getProduct);
router.get('/find/:id', productController.getProductId);

router.post('/', productController.postProduct);

router.put('/:id', productController.putProduct);

router.delete('/:id', productController.deleteProduct);

router.get('/', auth.verifyToken, productController.getIndexProduct);

module.exports = router;