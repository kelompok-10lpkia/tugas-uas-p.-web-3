const express = require('express');

const bodyParser = require('body-parser');

const app = express();

app.set('view engine', 'ejs');
app.use(bodyParser.json());

const homeRouter = require('./routes/home');

const sequelize = require('./configs/sequelize');

const Product = require('./models/product');

const Cart = require('./models/cart');

const productRouter = require('./routes/product');
const userRouter = require('./routes/User');
const cartRouter = require('./routes/cart');

const User = require('./models/User');

app.use('/', homeRouter);
app.use('/product', productRouter);
app.use('/cart', cartRouter);
app.use('/user', userRouter);

app.listen(3310, () => {
    console.log('server started 3310');
    sequelize.sync();
})